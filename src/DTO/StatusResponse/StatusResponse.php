<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 09.10.2015
 * Time: 17:43
 */
namespace PeakCase\DTO\StatusResponse;

class StatusResponse implements StatusResponseInterface
{
    public $status;
    public $statusCode;
    public $errors = array();
    public $message;
    public $data = array();

    public function __construct($status, $message ='', $data = array(), $errors = array(), $statusCode ='')
    {
        $this->setStatus($status);
        $this->setMessage($message);
        $this->setData($data);
        $this->setErrors($errors);
        $this->setStatusCode($statusCode);
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public function setErrors($errors)
    {
        if(is_array($errors))
        {
            foreach($errors as $error)
            {
                $this->errors[] = $error;
            }
        }
        else
        {
            $this->errors[] = $errors;
        }
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }
}