<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 09.10.2015
 * Time: 17:45
 */
namespace PeakCase\DTO\StatusResponse;

interface StatusResponseInterface
{
    public function setStatus($status);

    public function setStatusCode($statusCode);

    public function setErrors($errors);

    public function setMessage($message);

    public function setData($data);

    public function getStatus();

    public function getStatusCode();

    public function getErrors();

    public function getMessage();

    public function getData();
}