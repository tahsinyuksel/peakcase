<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 00:47
 */

namespace PeakCase\Models;


use System\BaseModel;

class GiftOperations extends BaseModel
{
    protected $table = 'userGifts';

    public function __construct()
    {
        parent::__construct();
    }

    public function getUserInboxGifts($userId)
    {
        return $this->getBoxGifts($userId, 'inbox');
    }

    public function getUserSendGifts($userId)
    {
        return $this->getBoxGifts($userId, 'sendbox');
    }

    private function getBoxGifts($userId, $type = 'sendbox')
    {
        $whereUser = 'sourceUserId';
        $selectUser = 'targetUserId';
        if($type == 'inbox'){
            $whereUser = 'targetUserId';
            $selectUser = 'sourceUserId';
        }

        $date = new \DateTime();
        $query = $this->db->prepare(
            'SELECT UG.id, UG.created, U.id AS userId, U.name AS userName, G.name AS giftName
            FROM '. $this->table .' AS UG
            INNER JOIN users AS U
            ON U.id = UG.'.$selectUser.'
            INNER JOIN gift AS G
            ON G.id = UG.giftId
            WHERE UG.'.$whereUser.' = :userId AND UG.created <= :currentDate AND UG.expire > :currentDate
            ORDER BY UG.created DESC
            '
        );
        $query->execute(
            array(
                'userId'=> $userId,
                'currentDate'=> $date->format('Y-m-d H:i:s')
            )
        );
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);

        if($query->rowCount() > 0){
            return $result;
        }
        return null;
    }

    public function getGiftsByToday($sourceUserId, $targetUserId)
    {
        $date = new \DateTime();
        $query = $this->db->prepare(
            'SELECT * FROM '. $this->table .'
            WHERE sourceUserId = :sourceUserId AND targetUserId = :targetUserId AND created >= :startDate AND created < :endDate LIMIT 1'
        );
        $query->execute(
            array(
                'sourceUserId'=> $sourceUserId,
                'targetUserId'=> $targetUserId,
                'startDate'=> $date->format('Y-m-d 00:00:00'),
                'endDate'=> $date->format('Y-m-d 23:59:59')
            )
        );
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount() == 1){
            return $result;
        }
        return null;
    }

    public function getAvailableGiftUsers($userId)
    {
        $date = new \DateTime();
        $query = $this->db->prepare(
            'SELECT * FROM users WHERE id != :userId AND id NOT IN
            (SELECT targetUserId FROM '. $this->table .'
            WHERE sourceUserId = :userId AND created >= :startDate AND created < :endDate)'
        );
        $query->execute(
            array(
                'userId'=> $userId,
                'startDate'=> $date->format('Y-m-d 00:00:00'),
                'endDate'=> $date->format('Y-m-d 23:59:59')
            )
        );
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        if($query->rowCount() > 0){
            return $result;
        }
        return null;
    }
}