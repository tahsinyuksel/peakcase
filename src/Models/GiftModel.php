<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 17:14
 */

namespace PeakCase\Models;


use System\BaseModel;

class GiftModel extends BaseModel implements GiftInterface
{
    public $id;
    public $name;
    public $category = 'general';
    public $type = 'default';
    public $price = 0;

    private $table = 'gift';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $query = $this->db->prepare('SELECT * FROM '. $this->table .' WHERE id = ? LIMIT 1');
        $query->execute(array($id));
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount() == 1){
            return $result;
        }
        return null;
    }

    public function all()
    {
        return $this->db->query('SELECT * FROM ' . $this->table, \PDO::FETCH_ASSOC)->fetchAll();
    }

    public function create($data = array())
    {
        $query = $this->db->prepare('INSERT INTO '. $this->table . '(name, category, price, type)  VALUES(:name, :category, :price, :type)');
        if(! empty($data)){
            return $query->execute($data);
        }
        else
        {
            return $query->execute(
                array(
                    'name'=>$this->name,
                    'category' => $this->category,
                    'price' => $this->price,
                    'type' => $this->type
                )
            );
        }
    }

    public function save($data)
    {

    }
}