<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 17:14
 */

namespace PeakCase\Models;


use System\BaseModel;

class UserModel extends BaseModel implements UserInterface
{
    public $id;
    public $name;
    public $points = 0;
    public $price = 0;
    public $fbId = '';

    private $table = 'users';

    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $query = $this->db->prepare('SELECT * FROM users WHERE id = ? LIMIT 1');
        $query->execute(array($id));
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount() == 1){
            return $result;
        }
        return null;
    }

    public function findByFb($id)
    {
        $query = $this->db->prepare('SELECT * FROM users WHERE fbId = ? LIMIT 1');
        $query->execute(array($id));
        $result = $query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount() == 1){
            return $result;
        }
        return null;
    }

    public function all()
    {
        return $this->db->query('SELECT * FROM users', \PDO::FETCH_ASSOC);
    }

    public function create($data = array())
    {
        $query = $this->db->prepare('INSERT INTO '. $this->table . '(name, price, points, fbId)  VALUES(:name, :price, :points, :fbId)');
        if(! empty($data)){
            return $query->execute($data);
        }
        else
        {
            return $query->execute(
                array(
                    'name'=>$this->name,
                    'price' => $this->price,
                    'points' => $this->points,
                    'fbId' => $this->fbId
                )
            );
        }
    }

    public function setPrice($price)
    {
        $this->price = abs($price);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function save($user)
    {
        $query = "UPDATE ". $this->table ."
            SET name = :name,
            price = :price,
            points = :points,
            fbId = :fbId
            WHERE id = :id";
        $stmt = $this->db->prepare($query);

        if(is_object($user) && $user instanceof UserModel){

            $data = array(
                'id' => $user->id,
                'name' => $user->name,
                'price' => $user->price,
                'points' => $user->points,
                'fbId' => $user->fbId
            );
            return $stmt->execute($data);
        }
        else
        {
            return $stmt->execute($user);
        }
    }

    /**
     * bu sekilde kullanicinin gonderdigi ve gelen hediyelerinin listesinide alabilirdik.
     * bu islemler icin ilgili sinif UserGifts kullanildi.
     * @see UserGifts
     */
    /*
    private function getSendGiftList()
    {

    }

    private function getInGiftList()
    {

    }
    */
}