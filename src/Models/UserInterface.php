<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 18:54
 */

namespace PeakCase\Models;


interface UserInterface extends CrudInterface
{
    public function setPrice($price);

    public function getPrice();
}