<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 17:38
 */

namespace PeakCase\Models;


class UserGifts extends GiftOperations
{
    public $targetUserId;

    public $sourceUserId;

    public $giftId;

    private $expire;

    private $created;

    public function __construct($sourceId, $targetId, $giftId, $expire = '', $created = '')
    {
        $this->sourceUserId = $sourceId;
        $this->targetUserId = $targetId;
        $this->giftId       = $giftId;
        $this->expire       = $expire == '' ? date('Y-m-d H:i:s', strtotime('+2 week')) : $expire;
        $this->created      = $created == '' ? date('Y-m-d H:i:s', strtotime('now')) : $created;

        parent::__construct();
    }

    public function setExpire($expire)
    {
        $this->expire = $expire;
    }

    /**
     * @return bool
     */
    public function send()
    {
        ##  users control. is user?
        ##  gift control. is gift?
        ##  gift coins <= user price control
        ##  is user send 1 gift per day control
        ##  send gift

        $query = $this->db->prepare(
            'INSERT INTO '. $this->table . '(sourceUserId, targetUserId, giftId, expire, created)
            VALUES(:sourceUserId, :targetUserId, :giftId, :expire, :created)'
        );

        return $query->execute(
            array(
                'sourceUserId'=>$this->sourceUserId,
                'targetUserId' => $this->targetUserId,
                'giftId'=>$this->giftId,
                'expire' => $this->expire,
                'created' => $this->created
            )
        );
    }
}