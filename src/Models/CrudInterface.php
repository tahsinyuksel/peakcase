<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 18:53
 */

namespace PeakCase\Models;


interface CrudInterface
{
    public function find($id);

    public function all();

    public function create($data = array());

    public function save($data);
}