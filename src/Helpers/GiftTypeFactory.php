<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 21.12.2015
 * Time: 01:17
 */

namespace PeakCase\Helpers;


class GiftTypeFactory
{
    public static function create($type = '', $sourceUser, $targetUser, $gift, $userGifts = null)
    {
        $instance = null;
        switch (strtolower($type))
        {
            case 'default':
                $instance = new DefaultGiftType($sourceUser, $targetUser, $gift, $userGifts);
                break;

            case 'coins':
                $instance = new CoinsGiftType($sourceUser, $targetUser, $gift, $userGifts);
                break;

            default:
                //throw new \BadMethodCallException;
                $instance = new DefaultGiftType($sourceUser, $targetUser, $gift, $userGifts);
                break;
        }

        return $instance;
    }
}