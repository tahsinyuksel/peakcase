<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 18:36
 */

namespace PeakCase\Helpers;

use PeakCase\DTO\StatusResponse\StatusResponse;
use PeakCase\Models\GiftInterface;
use PeakCase\Models\GiftOperations;
use PeakCase\Models\UserInterface;
use System\BaseHelper;

class UserGiftService extends BaseHelper
{
    public function sendGift($sourceUserId, $targetUserId, $giftId)
    {
        $sourceUser = null;
        $targetUser = null;
        $gift       = null;

        $userModel = $this->load->model('user');
        $giftModel = $this->load->model('gift');

        /**
         * user controls
         * ------------------------ begin ------------------------
         */

        if($userModel instanceof UserInterface)
        {
            $sourceUser = $userModel->find($sourceUserId);
            $targetUser = $userModel->find($targetUserId);

            if($sourceUser == null){
                return new StatusResponse('error', 'Kullanıcı bulunamadı. Oturum açılmalı.', array(), array('Kullanıcı bulunamadı. Oturum açılmalı.', 'SourceUser404'));
            }
            if($targetUser == null){
                return new StatusResponse('error', 'Kullanıcı bulunamadı.', array(), array('Kullanıcı bulunamadı.'), 'TargetUser404');
            }
        }

        /**
         * gift controls
         * ------------------------ begin ------------------------
         */
        if($giftModel instanceof GiftInterface)
        {
            $gift = $giftModel->find($giftId);
            if($gift == null){
                return new StatusResponse('error', 'Hediye bulunamadı.', array(), array('Hediye bulunamadı.'), 'Gift404');
            }
        }

        $giftType = GiftTypeFactory::create($gift['type'], $sourceUser, $targetUser, $gift);
        $result = $giftType->send();

        /**
         * send notification in the facebook
         * burasi kuyruk isleyici veya asenkron olarak da yapilabilir
         * ------------------------ begin ------------------------
         */
        if($result->getStatus() == 'success')
        {
            if(isset($targetUser['fbId']))
            {
                $this->sendGiftNotifications($targetUser['fbId'], $gift['name']);
            }
        }

        return $result;
    }

    /**
     * The gifts sent by users
     * @param $userId
     * @return StatusResponse
     */
    public function getUserSendGifts($userId)
    {
        return $this->getBoxGiftList($userId, 'sendbox');
    }

    /**
     * Gift inbox by users
     * @param $userId
     * @return StatusResponse
     */
    public function getUserInboxGifts($userId)
    {
        return $this->getBoxGiftList($userId, 'inbox');
    }

    private function getBoxGiftList($userId, $type = 'inbox')
    {
        if(! $userId)
        {
            return new StatusResponse('error', 'Kullanıcı bulunamadı.', array(), array('Kullanıcı bulunamadı.'), 'SendGiftList403');
        }

        $giftOperations = new GiftOperations();
        if($type == 'sendbox')
        {
            $result = $giftOperations->getUserSendGifts($userId);
        }else
        {
            $result = $giftOperations->getUserInboxGifts($userId);
        }

        return new StatusResponse('success', 'Gönderilen Hediyeler.', $result, array(), $type . 'GiftList200');
    }

    /**
     * Kullanicinin hediye gonderebilecegi kullanici listesi.
     * Bugun hediye gonderdigi kullanicilar ve kendisi haric
     * @param $userId
     * @return StatusResponse
     */
    public function getAvailableGiftUsers($userId)
    {
        $giftOperations = new GiftOperations();
        $result = $giftOperations->getAvailableGiftUsers($userId);
        return new StatusResponse('success', 'Gönderilen Hediyeler.', $result, array(), 'AvailableList200');
    }

    /**
     * @param $userId
     * @param string $giftName
     * @return bool
     */
    public function sendGiftNotifications($userId, $giftName = '')
    {
        if($userId != '')
        {
            $template = 'Sana bir %s hediye gonderildi';

            if(isset($_SESSION['user']['fbId']) && $_SESSION['user']['fbId'] != '')
            {
                $template = '@['. $_SESSION['user']['fbId'] .'] sana bir hediye %s gonderdi.';
            }

            $template = sprintf($template, "($giftName)");
            $fb = new FacebookHelper();
            $fb->sendNotification($userId, $template);
            return true;
        }
        return false;
    }
}