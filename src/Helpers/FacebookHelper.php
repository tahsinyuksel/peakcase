<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 26.12.2015
 * Time: 22:50
 */

namespace PeakCase\Helpers;

use Facebook;

class FacebookHelper
{
    public $fb;
    private $debug = true;

    private $appId      = '783126051815675';
    private $appSecret  = 'b590172cdbf9f705052f88c28ff78cda';
    private $appVersion = 'v2.5';


    public function __construct()
    {
        $this->fb = new Facebook\Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => $this->appVersion,
            //'default_access_token' => '{access-token}', // optional
        ]);

    }

    public function getMe()
    {
        $accessToken = $this->login();
        if($accessToken != '')
        {
            try {
                $response = $this->fb->get('/me', $accessToken);
                $userNode = $response->getGraphUser();
                return $userNode->asArray();
            } catch(Facebook\Exceptions\FacebookResponseException $e) {
                $this->errorResponse('Graph returned an error: ' . $e->getMessage());
            } catch(Facebook\Exceptions\FacebookSDKException $e) {
                $this->errorResponse('Facebook SDK returned an error: ' . $e->getMessage());
            }
        }
        return null;
    }

    public function sendNotification($userId, $template = '')
    {
        $accessToken = $this->appId . '|' . $this->appSecret;
        try {
            $this->fb->post($userId . '/notifications?access_token='. $accessToken . '&template=' . $template);
            return true;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            $this->errorResponse('Graph returned an error: ' . $e->getMessage());
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            $this->errorResponse('Facebook SDK returned an error: ' . $e->getMessage());
        }
        return false;
    }

    public function login()
    {
        $helper = $this->fb->getJavaScriptHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $this->errorResponse('Graph returned an error: ' . $e->getMessage());
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $this->errorResponse('Facebook SDK returned an error: ' . $e->getMessage());
        }

        if (isset($accessToken)) {
            // Logged in!
            $_SESSION['facebook_access_token'] = (string) $accessToken;

            // Now you can redirect to another page and use the
            // access token from $_SESSION['facebook_access_token']
            return (string) $accessToken;
        }

        return '';
    }

    private function errorResponse($err)
    {
        if($this->debug == true)
        {
            echo $err;
            exit;
        }
    }
}