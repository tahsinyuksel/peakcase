<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 21.12.2015
 * Time: 01:28
 */

namespace PeakCase\Helpers;


interface GiftTypeInterface
{
    public function send();

    public function validations();

    public function setExpire($expire);
}