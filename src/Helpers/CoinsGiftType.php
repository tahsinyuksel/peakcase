<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 21.12.2015
 * Time: 00:50
 */

namespace PeakCase\Helpers;


use PeakCase\DTO\StatusResponse\StatusResponse;
use PeakCase\Models\GiftModel;
use PeakCase\Models\UserGifts;
use PeakCase\Models\UserModel;

class CoinsGiftType implements GiftTypeInterface
{
    public $sourceUser = null;
    public $targetUser = null;
    public $gift       = null;
    public $userGifts  = null;

    private $expire;
    private $userModel;
    private $giftModel;

    public function __construct($sourceUser, $targetUser, $gift, $userGifts = null)
    {
        $this->sourceUser = $sourceUser;
        $this->targetUser = $targetUser;
        $this->gift = $gift;
        if($userGifts == null)
        {
            $this->userGifts = new UserGifts($sourceUser['id'], $targetUser['id'], $gift['id']);
        }

        $this->userModel = new UserModel();
        $this->giftModel = new GiftModel();
    }

    public function setExpire($expire)
    {
        $this->expire = $expire;
        $this->userGifts->setExpire($expire);
    }

    public function send()
    {
        /**
         * is error ?
         * ------------------------ begin ------------------------
         */
        $validationStatus = $this->validations();
        if($validationStatus->getStatus() == 'error')
        {
            return $validationStatus;
        }/* ------------------------ end ------------------------*/

        /**
         * send gift
         * ------------------------ begin ------------------------
         */
        if($this->userGifts->send())
        {
            /* ------------------------ user price reload ------------------------*/
            $total = intval($this->sourceUser['price']) - intval($this->gift['price']);
            $this->sourceUser['price'] = $total;
            $this->userModel->save($this->sourceUser);
            /* ------------------------ end ------------------------*/

            /* ------------------------ target user price add ------------------------*/
            $this->targetUser['price'] += $this->gift['price'];
            $this->userModel->save($this->targetUser);
            /* ------------------------ end ------------------------*/

            return new StatusResponse('success', 'Hediye Gönderildi.', array('targetUser'=>$this->targetUser), array(), 'UserGiftSend200' );
        }
        return new StatusResponse('error', 'İşlem yapılamadı.', array(), array('İşlem yapılamadı.'), 'UserGiftSend500');
    }
    
    public function validations()
    {
        /**
         * user controls
         * ------------------------ begin ------------------------
         */
        if($this->targetUser['id'] == $this->sourceUser['id'])
        {
            return new StatusResponse('error', 'Kendinize hediye gönderemezsiniz.', array(), array('Kendinize hediye gönderemezsiniz.', 'User403'));
        }

        if($this->sourceUser == null)
        {
            return new StatusResponse('error', 'Kullanıcı bulunamadı. Oturum açılmalı.', array(), array('Kullanıcı bulunamadı. Oturum açılmalı.', 'SourceUser404'));
        }

        if($this->targetUser == null)
        {
            return new StatusResponse('error', 'Kullanıcı bulunamadı.', array(), array('Kullanıcı bulunamadı.'), 'TargetUser404');
        }

        /**
         * gift coins <= user price control
         * ------------------------ begin ------------------------
         */
        if($this->sourceUser['price'] < $this->gift['price'])
        {
            return new StatusResponse('error', 'Hediye için paranız yeterli değil.', array(), array('Hediye için paranız yeterli değil.'), 'SourcePrice403');
        }

        /**
         * is user send 1 gift per day control
         * ------------------------ begin ------------------------
         */
        $isToday = $this->userGifts->getGiftsByToday($this->sourceUser['id'], $this->targetUser['id']);
        if($isToday != null)
        {
            return new StatusResponse('error', 'Hediye gönderilemez', array(), array('Hediye gönderilemez. Bugün bu kullanıcıya hediye gönderilmiş.'), 'GiftSend403');
        }

        return new StatusResponse('success', 'Hediye gönderilebilir.');
    }
}