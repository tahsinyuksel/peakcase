<div class="row">
    <div class="col-sm-12">

        <h2>Anasayfa</h2>

        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">

                <?php if(isset($_SESSION['user'])) {?>
                    <a href="/user/profile">Profil</a>
                <?php
                } else{ ?>

                <form role="form" action="user/login/" method="post">
                    <div class="form-group">
                        <label for="name">Username:</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" name="password" id="password">
                    </div>
                    <button type="submit" class="btn btn-default">Giriş Yap</button>
                    <button type="button" class="btn btn-default" onclick="fbLogin();">Facebook Login</button>
                    <a href="/user/create/" class="btn btn-default">Kaydol</a>
                </form>

                <?php } ?>

            </div>
        </div>

    </div>
</div>

<script>

    function fbLogin() {
        FB.login(function(response) {

            if (response.status === 'connected') {
                window.location.href = '/user/fbLogin/'
            }

        }, {scope: 'public_profile'});
    }

</script>