<div class="row">
    <div class="col-sm-12">

        <?php
        if(isset($type) && $type == 'inbox'){
            $title = 'Gelen Hediyeler';
            $userTitle = 'Gönderen';
        }else{
            $title = 'Gönderilen Hediyeler';
            $userTitle = 'Gönderilen';
        }
        ?>
        <h2><?php echo $title?></h2>

        <div class="row">
            <div class="col-sm-4">
                <b><?php echo $userTitle?></b>
            </div>
            <div class="col-sm-4">
                <b>Hediye</b>
            </div>
            <div class="col-sm-4">
                <b>Gönderilme Tarihi</b>
            </div>
        </div>

        <?php
        if(isset($resultList)){
            foreach ($resultList as $resultData) {?>
                <div class="row">
                    <div class="col-sm-4">
                        <?php echo $resultData['userName']; ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $resultData['giftName']; ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo $resultData['created']; ?>
                    </div>
                </div>
            <?php
            }
        }
        ?>

    </div>
</div>
