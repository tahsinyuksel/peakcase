<div class="row">
    <div class="col-sm-12">

        <h2>Hediyeler</h2>

        <div class="row">
            <div class="col-sm-3">
                <b>Id</b>
            </div>
            <div class="col-sm-3">
                <b>Name</b>
            </div>
            <div class="col-sm-3">
                <b>Price</b>
            </div>
            <div class="col-sm-3">
                <b>Type</b>
            </div>
        </div>

            <?php
            if(isset($giftList)){
                foreach ($giftList as $giftData) {?>
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo $giftData['id']; ?>
                        </div>
                        <div class="col-sm-3">
                            <?php echo $giftData['name']; ?>
                        </div>
                        <div class="col-sm-3">
                            <?php echo $giftData['price']; ?>
                        </div>
                        <div class="col-sm-3">
                            <?php echo $giftData['type']; ?>
                        </div>
                    </div>
                <?php
                }
            }
            ?>

    </div>
</div>
