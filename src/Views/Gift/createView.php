<div class="row">
    <div class="col-sm-12">

        <h3>Hediye Ekle</h3>

        <form role="form" action="gift/create" method="post">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="category">Category:</label>
                <input type="text" class="form-control" name="category" id="category" placeholder="Kullanilmiyor">
            </div>
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="text" class="form-control" name="price" id="price" placeholder="Hediye Degeri, 15">
            </div>
            <div class="form-group">
                <label for="type">Type:</label>
                <select name="type" class="form-control">
                    <option value="default">Default</option>
                    <option value="coins">Coins</option>
                </select>

            </div>
            <button type="submit" class="btn btn-default">Kaydet</button>
        </form>

    </div>
</div>