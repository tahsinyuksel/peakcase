<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>PeakCase</title>

        <!-- Bootstrap -->
        <link href="<?php echo assets('bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
        <link href="<?php echo assets('bootstrap/dist/css/bootstrap-theme.min.css')?>" rel="stylesheet">
        <link href="<?php echo assets('css/app.css')?>" rel="stylesheet">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="<?php echo assets('bootstrap/dist/js/bootstrap.min.js')?>"></script>
        <script src="<?php echo assets('js/app.js')?>"></script>
    </head>
    <body>

    <div class="container">

        <!-- Static navbar -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">PeakCase</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li><a href="https://bitbucket.org/tahsinyuksel/peakcase/src/master/doc/doc.md">About</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kullanıcılar <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/user/create/">Ekle</a></li>
                                <li><a href="/user/list/">Listele</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hediyeler <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/gift/create/">Ekle</a></li>
                                <li><a href="/gift/list/">Listele</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php if(isset($_SESSION['user']) && !empty($_SESSION['user'])) {?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menüm <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/user/profile/">Profil</a></li>
                                    <li><a href="/gift/sendGift/">Hediye Gönder</a></li>
                                    <li><a href="/gift/boxGifts/?userId=<?php echo $_SESSION['user']['id']?>&type=inbox">Gelen Hediyeler</a></li>
                                    <li><a href="/gift/boxGifts/?userId=<?php echo $_SESSION['user']['id']?>&type=sendbox">Gönderilen Hediyeler</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/user/logout/">Çıkış Yap</a></li>
                                </ul>
                            </li>
                        <?php } else {?>
                            <li><a href="/">Giriş Yap</a></li>
                        <?php }?>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <!-- Main component for a primary marketing message or call to action -->
        <div class="jumbotron">
            <h1>PeakCase Application</h1>
            <p>Online Uygulama. Tüm işlevlere buradan erişebilirsiniz. Hakkında daha fazla bilgi ve döküman için;</p>
            <p>
                <a class="btn btn-lg btn-primary" href="https://bitbucket.org/tahsinyuksel/peakcase/src/master/doc/doc.md" role="button">Dökümanı görüntüle »</a>
                <a class="btn btn-lg btn-primary" href="javascript:facebookInviteFriends();" role="button">Arkadaş Davet Et</a>
            </p>
        </div>


        <?php

        $_templatePageView = isset($_templatePageView) ? $_templatePageView : '';

        if(isset($errors) && !empty($errors)){

            foreach ($errors['errors'] as $error) {?>
                <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
            <?php
            }
        }

        if(isset($process) && $process == 'success'){

            $successMessage = isset($successMessage) ? $successMessage : 'İşlem Başarılı';
            ?><div class="alert alert-success" role="alert"><?php echo $successMessage; ?></div><?php
        }

        include $_templatePageView;
        ?>

    </div>

    <footer class="footer">
        <div class="container">
            <p class="text-muted">PeakCase App.</p>
        </div>
    </footer>

    </body>
</html>


