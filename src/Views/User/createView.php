<div class="row">
    <div class="col-sm-12">

        <h3>Kullanıcı Ekle</h3>

        <form role="form" action="user/create/" method="post">
            <div class="form-group">
                <label for="name">Username:</label>
                <input type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="name">Price:</label>
                <input type="text" class="form-control" name="price" id="price" value="0">
            </div>
            <button type="submit" class="btn btn-default">Kaydet</button>
            <fb:login-button onlogin="login();">Üye ol</fb:login-button>
        </form>




    </div>
</div>

<div id="status">
</div>

<script>

    function login() {
        FB.login(function(response) {

            if (response.status === 'connected') {
                createUser();
            }

        }, {scope: 'public_profile'});
    }

    function createUser(){

        FB.api('/me', function(response) {
            console.log(response);

            $.post('/user/create/', {'name': response.name, 'fbId': response.id}, function(data, status){
                console.log(data);
                if(data && status == 'success' && data['process'] == 'success'){
                    alert('Kullanıcı eklendi.');
                } else{
                    var errMsg = data['errors']['message'] + '\n\r';
                    if(data['errors']['errors']){
                        $.each(data['errors']['errors'], function(errI, err){
                            errMsg += err + '\n\r';
                        });
                    }

                    alert(errMsg);
                }
            });
        });
    }

</script>