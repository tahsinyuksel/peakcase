<div class="row">
    <div class="col-sm-12">

        <h2>Kullanıcılar</h2>

        <div class="row">
            <div class="col-sm-1">
                Id
            </div>
            <div class="col-sm-3">
                Name
            </div>
            <div class="col-sm-3">
                Price
            </div>
            <div class="col-sm-3">
                Points
            </div>
            <div class="col-sm-2">
                İşlem
            </div>
        </div>

        <?php
        if(isset($userList)){
            $activeUserId = isset($_SESSION['user']) ?  $_SESSION['user']['id'] : 0;
            foreach ($userList as $userData) {?>
                <div class="row">
                    <div class="col-sm-1">
                        <?php echo $userData['id']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['name']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['price']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['points']; ?>
                    </div>
                    <div class="col-sm-2">

                        <?php if($activeUserId != $userData['id']) {?>
                            <button class="btn btn-primary" id="sendBtn<?php echo $userData['id']; ?>" onclick="giftLoadModal('<?php echo $userData['id']; ?>')" data-toggle="modal" data-target="#giftSendModal">Hediye Gönder</button>
                        <?php } ?>

                    </div>
                </div><br>
            <?php
            }
        }else { ?>

        <div class="row text-center">
            Boş
        </div>

        <?php  } ?>

    </div>
</div>

<div class="modal fade" id="giftSendModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Hediye Gönder</h4>
            </div>
            <div class="modal-body">
                <p>Hediyeler</p>
                <div class="">
                    <div class="row giftRow" id="">
                        <div class="col-sm-3 giftRowId">
                            <b>Name</b>
                        </div>
                        <div class="col-sm-3 giftRowId">
                            <b>Price</b>
                        </div>
                        <div class="col-sm-3 giftRowName">
                            <b>Category</b>
                        </div>
                        <div class="col-sm-3 giftRowType">
                            <b>Type</b>
                        </div>
                    </div>
                    <div id="giftListModalDiv"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                <button type="button" id="giftSendModalBtn" class="btn btn-primary">Gönder</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    loadModalGiftList();

    $('#giftSendModalBtn').click(function(){
        var giftId = $('input[name=giftId]:checked', '#giftSendModal').val();
        var targetUserId = $('#giftSendModalBtn').attr('data-val');
        if(giftId && giftId > 0){

            var val = {'targetUserId': targetUserId, 'giftId': giftId};
            $.post('/gift/send/', val, function (data, status) {
                if(data['process'] == 'success'){
                    $('#sendBtn' + targetUserId).addClass('disabled');
                    $('#sendBtn' + targetUserId).attr('onclick','');
                    $('#sendBtn' + targetUserId).attr('data-target','');
                    $('#sendBtn' + targetUserId).html('Gönderildi');

                    console.log(data['data']['targetUser']);

                    sendRequest(data['data']['targetUser']['fbId']);

                    alert('Hediye Gönderildi.');
                }else{
                    var errMsg = data['errors']['message'] + '\n\r';
                    if(data['errors']['errors']){
                        $.each(data['errors']['errors'], function(errI, err){
                            errMsg += err + '\n\r';
                        });
                    }

                    alert(errMsg);
                }
            })

        }else{
            alert('Hediye seçiniz.');
        }

    });

    /**
     * istenilirse gonderilen kullaniciya istek yolla. param: to
     */
    function sendRequest(userId){
        FB.ui({method: 'apprequests',
            message: 'PeakCase Oyna, Hediye yolla kazan.',
            title: 'İstek Gönder'
            //to: userId
        }, function(response){
            console.log(response);
        });
    }

    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                return FB.getAuthResponse();
            }
        });
        return null;
    }

    function giftLoadModal(userId)
    {
        $('#giftSendModalBtn').attr('data-val', userId);
    }

    function loadModalGiftList()
    {
        $.get("/gift/list/", function(data, status){

            if(status == 'success'){

                //var div = $('.giftRow').clone();
                $.each( data, function( key, value ) {

                    /**
                     * 2.method tek tek html eklemek yerine clone al,degisiklikleri uygula
                     * 3.method html response
                     * $(div).find('.giftRowName').html(value['name']);
                     */

                    var html = '';
                    html += '<div class="row">';
                        html += '<div class="col-sm-12">';
                            html += '<div class="col-sm-3">';
                            html += '<input type="radio" name="giftId" id="giftId" value="'+ value['id'] +'">';
                            html += value['name'];
                            html += '</div>';
                            html += '<div class="col-sm-3">'+ value['price'] +'</div>';
                            html += '<div class="col-sm-3">'+ value['category'] +'</div>';
                            html += '<div class="col-sm-3">'+ value['type'] +'</div>';
                        html += '</div>';
                    html += '</div>';
                    $('#giftListModalDiv').append(html);
                });

            }
        });
    }
</script>