<div class="row">
    <div class="col-sm-12">

        <h2>Kullanıcılar</h2>

        <div class="row">
            <div class="col-sm-1">
                Id
            </div>
            <div class="col-sm-3">
                Name
            </div>
            <div class="col-sm-3">
                Price
            </div>
            <div class="col-sm-3">
                Points
            </div>
            <div class="col-sm-2">
                İşlem
            </div>
        </div>

        <?php
        if(isset($userList)){
            $activeUserId = isset($_SESSION['user']) ?  $_SESSION['user']['id'] : 0;
            foreach ($userList as $userData) {?>
                <div class="row">
                    <div class="col-sm-1">
                        <?php echo $userData['id']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['name']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['price']; ?>
                    </div>
                    <div class="col-sm-3">
                        <?php echo $userData['points']; ?>
                    </div>
                    <div class="col-sm-2">

                        <a href="/user/detail/?id=<?php echo $userData['id']; ?>">Detay</a>

                    </div>
                </div><br>
            <?php
            }
        }else { ?>

        <div class="row">
            Boş
        </div>

        <?php  } ?>

    </div>
</div>