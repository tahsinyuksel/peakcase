<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 16:08
 */

namespace PeakCase\Controllers;

use System\BaseController;

class IndexController extends BaseController
{
    public function indexAction()
    {
        $this->loadView('Index/index');
    }
}