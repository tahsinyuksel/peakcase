<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 17:07
 */

namespace PeakCase\Controllers;


use PeakCase\Helpers\FacebookHelper;
use PeakCase\Models\UserModel;
use System\BaseController;
use System\Input;

class UserController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * User list
     */
    public function listAction()
    {
        $userModel = $this->loadModel('user');
        $result = array();
        if($userModel instanceof UserModel){
            $result = $userModel->all();

        }
        $this->loadView('User/userList', array('userList'=> $result));
    }

    public function detailAction()
    {
        $input = Input::instance();
        if($input->get('id') != false)
        {
            $model = new UserModel();
            if($model instanceof UserModel)
            {
                $result = $model->find($input->get('id'));

                $this->loadView('User/detail', array('userData'=> $result));
            }
        }
        else{
            $this->loadView('User/detail', array('errors'=> array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'))));
        }
    }

    public function createAction()
    {
        $input = Input::instance();
        $data = array();

        /**
         * is sent post
         * ------------------------ POST ------------------------
         */
        if($input->post() != false)
        {
            if($input->post('name') != false)
            {
                $isUser = false;
                $model = new UserModel();
                if($model instanceof UserModel)
                {
                    $model->name = $input->post('name');
                    $model->setPrice($input->post('price', 0));

                    /**
                     * is facebook register ?
                     */
                    if($input->post('fbId') != false)
                    {
                        $model->fbId = $input->post('fbId');
                        $isUser = $model->findByFb($input->post('fbId'));
                    }

                    if($isUser == false)
                    {
                        if($model->create())
                        {
                            $data['process'] = 'success';
                        }
                        else
                        {
                            $data['process'] = 'error';
                            $data['errors'] = array('message'=>'Kullanıcı oluşturulamadı.', 'errors'=> array('Kullanıcı oluşturulamadı.'));
                        }
                    }
                    else
                    {
                        $data['process'] = 'error';
                        $data['errors'] = array('message'=>'Kullanıcı daha önce kaydedilmiş.', 'errors'=> array('Kullanıcı daha önce kaydedilmiş.'));
                    }
                }
            }
            else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'));
            }
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('User/create', $data);
    }

    public function loginAction()
    {
        $input = Input::instance();
        $data = array();

        /**
         * is sent post
         * ------------------------ POST ------------------------
         */
        if($input->post() != false)
        {
            if($input->post('name') != false)
            {
                $model = new UserModel();
                if($model instanceof UserModel)
                {
                    $id = $input->post('name');
                    $user = $model->find($id);
                    if($user != null)
                    {
                        $_SESSION['user'] = $user;
                        $data['process'] = 'success';
                        $data['successMessage'] = 'Giriş yapıldı.' . $user['name'];
                        header('Location: /user/profile/');
                    }else
                    {
                        $data['process'] = 'error';
                        $data['errors'] = array('message'=>'Kullanıcı bulunamadı.', 'errors'=> array('Kullanıcı bulunamadı.'));
                    }
                }
            }
            else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'));
            }
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('Index/index', $data);
    }

    /**
     * facebook login
     */
    public function fbLoginAction()
    {
        $fb = new FacebookHelper();
        $userProfile = $fb->getMe();
        if($userProfile && isset($userProfile['id']))
        {
            $model = new UserModel();
            $user = $model->findByFb($userProfile['id']);
            if($user != null)
            {
                $_SESSION['user'] = $user;
                $data['process'] = 'success';
                $data['successMessage'] = 'Giriş yapıldı.' . $user['name'];
                header('Location: /user/profile/');
            }else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>'Kullanıcı kayıtlı olarak bulunamadı.', 'errors'=> array('Kullanıcı kayıtlı olarak bulunamadı. Kaydolmak için <a href="/user/create/">tıklayın</a>'));
            }
        }
        else
        {
            $data['process'] = 'error';
            $data['errors'] = array('message'=>'Kullanıcı doğrulanamadı', 'errors'=> array('Kullanıcı doğrulanamadı'));
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('Index/index', $data);
    }

    public function profileAction()
    {
        $this->loadView('User/profile');
    }

    public function logoutAction()
    {
        session_destroy();
        header('Location: /');
    }
}