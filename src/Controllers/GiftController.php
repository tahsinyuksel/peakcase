<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 17:07
 */

namespace PeakCase\Controllers;


use PeakCase\Helpers\UserGiftService;
use PeakCase\Models\GiftModel;
use System\BaseController;
use System\Input;

class GiftController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Gif list
     */
    public function listAction()
    {
        $model = new GiftModel();
        $result = array();
        if($model instanceof GiftModel){
            $result = $model->all();

        }

        if ($this->isAjaxRequest())
        {
           echo $this->jsonResponse($result);return;
        }

        $this->loadView('Gift/giftList', array('giftList'=> $result));
    }

    public function detailAction()
    {
        $input = Input::instance();
        if($input->get('id') != false)
        {
            $model = new GiftModel();
            if($model instanceof GiftModel)
            {
                $result = $model->find($input->get('id'));

                $this->loadView('Gift/detail', array('giftData'=> $result));
            }
        }
        else{
            $this->loadView('Gift/detail', array('errors'=> array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'))));
        }
    }

    public function createAction()
    {
        $input = Input::instance();
        $data = array();

        /**
         * is sent post
         * ------------------------ POST ------------------------
         */
        if($input->post() != false)
        {
            if($input->post('name') != false)
            {
                $giftModel = new GiftModel();
                if($giftModel instanceof GiftModel)
                {
                    $giftModel->name        = $input->post('name');
                    $giftModel->category    = $input->post('category', '');
                    $giftModel->price       = $input->post('price', '');
                    $giftModel->type       = $input->post('type', '');
                    if($giftModel->create())
                    {
                        $data['process'] = 'success';
                    }else
                    {
                        $data['process'] = 'error';
                        $data['errors'] = array('message'=>'Kullanıcı oluşturulamadı.', 'errors'=> array('Kullanıcı oluşturulamadı.'));
                    }
                }
            }
            else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'));
            }
        }

        $this->loadView('Gift/create', $data);
    }

    public function sendAction()
    {
        $input = Input::instance();
        $data = array();

        $activeUser = $this->getUser();
        if($activeUser == false)
        {
            $data['process'] = 'error';
            $data['errors'] = array('message'=>'Giriş Yapınız', 'errors'=> array('Giriş Yapınız'));
        }
        else
        {
            /**
             * is sent post
             * ------------------------ POST ------------------------
             */
            if($input->post() != false)
            {
                if($input->post('targetUserId') != false && $input->post('giftId') != false)
                {
                    $userGiftService = new UserGiftService();
                    $statusResponse = $userGiftService->sendGift(
                        $activeUser['id'], $input->post('targetUserId'), $input->post('giftId')
                    );
                    if($statusResponse->getStatus() == 'success')
                    {
                        $data['process'] = 'success';
                        $data['data'] = $statusResponse->getData();
                    }
                    else
                    {
                        $data['process'] = 'error';
                        $data['errors'] = array('message'=>$statusResponse->getMessage(), 'errors'=> $statusResponse->getErrors());
                    }
                }
                else
                {
                    $data['process'] = 'error';
                    $data['errors'] = array('message'=>'Parametreler eksik', 'errors'=> array('Parametreler eksik'));
                }
            }
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('Gift/send', $data);
    }

    /**
     * userId session ile sadece giris yapmis kullanici icin veriler getirilebilirdi.
     * Simdilik parametre ile alinabilir birakildi.Kolay Testler icin
     */
    public function boxGiftsAction()
    {
        $input = Input::instance();
        $data = array();

        if($input->get('userId') != false)
        {
            $userGiftService = new UserGiftService();
            $type = $input->get('type', 'inbox');
            $data['type'] = $type;
            if($type == 'sendbox')
            {
                $statusResponse = $userGiftService->getUserSendGifts($input->get('userId'));
            }
            else
            {
                $statusResponse = $userGiftService->getUserInboxGifts($input->get('userId'));
            }

            if($statusResponse->getStatus() == 'success')
            {
                $data['resultList'] = $statusResponse->getData();
            }
            else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>$statusResponse->getMessage(), 'errors'=> $statusResponse->getErrors());
            }
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('Gift/sendBoxGifts', $data);
    }

    public function sendGiftAction()
    {
        $data = array();

        $activeUser = $this->getUser();
        if($activeUser == false)
        {
            $data['process'] = 'error';
            $data['errors'] = array('message'=>'Giriş Yapınız', 'errors'=> array('Giriş Yapınız'));
        }
        else
        {
            $userGiftService = new UserGiftService();
            $statusResponse = $userGiftService->getAvailableGiftUsers($activeUser['id']);
            if($statusResponse->getStatus() == 'success')
            {
                $data['userList'] = $statusResponse->getData();
            }
            else
            {
                $data['process'] = 'error';
                $data['errors'] = array('message'=>$statusResponse->getMessage(), 'errors'=> $statusResponse->getErrors());
            }
        }

        if ($this->isAjaxRequest())
        {
            echo $this->jsonResponse($data);return;
        }

        $this->loadView('User/sendGift', $data);
    }
}