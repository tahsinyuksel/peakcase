<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'System\\' => array($baseDir . '/System'),
    'PeakCase\\' => array($baseDir . '/src'),
    'Facebook\\' => array($vendorDir . '/facebook/php-sdk-v4/src/Facebook'),
);
