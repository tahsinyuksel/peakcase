PeakCase
===
Kullanicilarin birbirlerine hediye gonderebilmesini saglayan uygulama.

# Kurallar:
-------
* Her kullanici tekil.
* Her kullanıcı diğer kullanıcılar için günde 1 hediye gönderebilir.
* Kullanıcılar sınırsız hediye alabilir.
* Hediyelerin 1 hafta sonra süresi dolar.

# Ozellikler:
-------
* Php5, OOP, 
* Basic other features; MVC, Design Pattern, Composer, Psr4 Autoload, Comments of code
* Mysql depolama, PDO
* Javascript asynchronous actions
* Source: https://bitbucket.org/tahsinyuksel/peakcase

# Ekstra
-------
* 1'den fazla hediye desteği
* her hediye icin fiyat,kategori gibi ozellikler saglandi
* 1'den fazla kullanici destegi
* facebook apps
* facebook kaydolma
* facebook davet
* facebook login(Facebook ile kaydolalan kullanici icin)
* hediye gönderildiginde facebook **Javascript SDK** ile uygulama isteği gönderme
* hediye gonderildiginde; gonderilen kullanici facebook ile kaydoldu ise hediye ile ilgili notification gonderme.
**(Facebook Php SDK)**
* Gelen ve Gonderilen Hediyeler
* Hazir/var olan bir framework **kullanilmadi**. 
* Uygulama akisini saglayacak, rahat gelistirme, belirli duzende ve anlasilir 
  sekilde uygulama gelistirebilecek basic **MVC Framework yapisi tasarlandi**
* Basit duzeyde fonksiyonelligi saglayacak kullanici arabirimi olusturuldu.(Bootstrap)
* Hediyeler icin ozellestirilebilir modul yapisi.(Orn: Hediye turu: coins,default gibi)
* Online: http://peakcase.linkle.net/ 

# Aciklama
-------

## Sistemin Tasarlanmasi:
Gereksinimler analiz edildikten sonra uygulamya gecildi.


Ilk oncelikli is; 
Projenin akisini saglayacak, rahat gelistirme, belirli duzende ve anlasilir sekilde uygulama gelistirebilecek basic 
MVC Framework yapisi tasarlandi.
Projede disiplinsiz, spagetti, daginik duzensiz kodlarin olusmasinin en basta onune gecerek temel duzeyde de olsa belirli 
ve temiz bir yapida uygulama gelisitirmek her projemde oldugu gibi ilk hedeflerimden biri oldu.
 Bu projeye ozel temel akisi saglayacak minimum duzeyde MVC yapisi uygulandı.
 
 Duzenli bir dizin yapisi olusturuldu. 
 Namespace yapisina uygun, PSR-4 Autoloading kullanarak uygulamanin calismasi ve kodlarin dahil edilmesi saglandi.
 
 Genel yazilim gelistirme ilkelerimde, Kod icinde aciklama satirlarina yer vermeye dokuman yapisina uygun gelistirmeye calisilir.
 
 Temiz anlasilir kod yazmaya ozen gosterildi.
 
 Veri tabanı Mysql PDO ile kullanildi. Model dosyalarindan database islemleri gerceklestirildi. Veri katmani soyutlamaya 
 ihtiyac duyulmadi.
 
## Uygulama
-------
 Urun/hediye ucretli veya ucertsiz olabilir.
 Hediye turleri icin farkli operasyonlar gerceklesebilecegi dusunulerek tur/tip bazinda genisletildi.
 Ornegin hediye turleri; coins(oyun ici para), default(klasik normal hediye) baz alindi.
 
 1. Bu seceneklerden default turunde; hediyenin bir tutari/degeri varsa kullanicinin bakiyesinin karsilayabilmesi beklenir.
 Uygunluk sagliyor ise kisinin bakiyesinden tutar dusulur hediye karsi tarafa gonderilir.
 
 2. Bu seceneklerden coins turunde; kisi sectigi kullaniciya coins(oyun ici para) gonderir. Hediye fiyati gonderen kullanici 
 bakiyesinden duserek, gonderilen kullanici bakiyesine coins olarak aktarilir. 
 
 coins turunde default' dan farkli olarak gonderilen kullanici bakiyesine eklenme/arttirma islevi varidir.
 Bu gibi farkli hediye turlerinde farkli operasyonlar yapilabilecegi dusunulerek hediye turleri yazilim tarafinda ayrilmasi 
 uygun bulundu.
 
 Factory Design Pattern uygulayarak hediye turu interface ile soyutlarak hediye turleri olusturuldu. Operasyonlar hediyenin 
 turune gore o ture ait islemler ortak interface uzerinden gerceklesitirilmektedir.

Boylece farkli hediye turlerinde farkli operasyonlar yapilabilmesi saglandi. Yeni hediye turleri eklenerek kolayca genisletilebilir ve gelistirilebilir yapi kuruldu.
 
# Not
-------
 Tum sistem basit olarak tasarlandi. Her konuda(yazilim,tasarim,guvenlik,kullanilabilirlik vs) iyilestirme yapilabilir.
 
 Bir cok islem Javascript ile asenkron yapilabilirdi, Hediye gonderme bolumu icin simdilik yeterli goruldu.Kolayca cevirilebilir.
 
 Hediye turleri icin secilebilir alanlar, ve hediye kategorili sekilde olusturulabilirdi.
 
 Coins gonderiminde 2 hafta sonra hediyenin suresinin dolmasi tam olarak anlasilamadi. Istenilen seneryoya gore dahil edilebilir.
 Kullaniciya gecen coinsler ayri bir programatik duzende saklanarak buradaki hediye coins leri kullanip kullanmadigi 
 takip edilerek yine 2.hafta sonunda gecersiz kilinabilir. Veya farklı seneryo is plani uzerinden uygulanabilir.
 
# Kullanim
-------
## Kullanici olusturma
 Basit sekilde olusturma saglandi.
 Menu > Kullanicilar > Kullanici Ekle
 
 Kullanici adi ile veya facebook ile uye olarak kullanici olsuturabilirisiniz.
 
## Login Olma
 1. Basit sekilde hizli kullanim icin sadece kullanici id ile giris yapip test edilebilmesi saglandi.
 2. Facebook ile giris.(Sadece facebook ile daha once kayit olan kullanicilar icin)
 
### 1) Kullanici ID ile login olma
 Giris yapmak icin; kullanici adi kismina **user Id giriniz.**  
**Sifre kullanilmaz, girmenize gerek yoktur**
 
### 2) Facebook ile login olma
 facebook ile daha once kayit olan kullanicilar icin kullanilabilir.

## Menum (Kullanici Menusu)
Giris yapan kullanicilar icin gerceklestirebilecekleri islemlerin yer aldigi menu.
Bulunan Aksiyonlar:
* Hediye Gonder
* Gelen Hediyeler
* Gonderilen Hediyeler

# Baglantilar:

 **Test Adres**: http://peakcase.linkle.net/  
 **Kaynak Kod**: https://bitbucket.org/tahsinyuksel/peakcase
