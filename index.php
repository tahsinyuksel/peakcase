<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 15:01
 */

include 'vendor/autoload.php';

define('ROOT',  (dirname(__FILE__)));
define ('DEVELOPMENT_ENVIRONMENT',true);

SetReporting();

function assets($file)
{
    return '/Web/' . $file;
}

function SetReporting() {
    if (DEVELOPMENT_ENVIRONMENT == true) {
        error_reporting(E_ALL);
        ini_set('display_errors','On');
    }
    else {
        error_reporting(E_ALL);
        ini_set('display_errors','Off');
    }
}

$app = new \System\App();
$app->run();