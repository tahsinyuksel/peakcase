<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 17:26
 */

namespace System;


class BaseModel
{
    /**
     * @var null|PDO
     */
    public $db;

    public function __construct()
    {
        $this->db = Db::getInstance();
    }
}