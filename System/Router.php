<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 15:35
 */

namespace System;


class Router
{
    const default_action = 'index';
    const default_controller = 'index';

    protected $request = array();

    public function __construct($url)
    {
        $this->SetRoute($url ? $url : self::default_controller);
    }

    /*
    *  The magic gets transforms $router->action into $router->GetAction();
    */
    public function __get($name)
    {
        if (method_exists($this, 'Get' . $name))
            return $this->{'Get' . $name}();
        else
            return null;
    }

    public function SetRoute($route)
    {
        $route = rtrim($route, '/');
        $route = ltrim($route, '/');
        $this->request = explode('/', $route);
    }

    public function GetAction()
    {
        if (isset($this->request[1]))
            return $this->request[1];
        else
            return self::default_action;
    }

    public function GetParams()
    {
        if (count($this->request) > 2)
        {
            $params = array_slice($this->request, 2);

            return $params;

        }else
        {
            return array();
        }
    }

    public function GetQueryParams()
    {
        $params = array();
        $parts = parse_url($_SERVER['REQUEST_URI']);
        if(isset($parts['query']))
        {
            parse_str($parts['query'], $query);
            foreach ($query as $key => $val) {
                $params[$key] = $val;
            }
        }
        return $params;
    }

    public function GetPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    public function GetController()
    {
        if (isset($this->request[0]) && !empty($this->request[0]))
            return $this->request[0];
        else
            return self::default_controller;
    }

    public function GetRequest()
    {
        return $this->request;
    }
}