<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 20.12.2015
 * Time: 18:44
 */

namespace System;

class BaseHelper
{
    /**
     * @var Load
     */
    public $load;

    public function __construct()
    {
        $this->load = new Load();
    }
}