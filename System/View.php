<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 20:59
 */

namespace System;


class View {

    protected $variables = array();
    protected $_controller;
    protected $_action;
    protected $_viewRoot;
    protected $_template;

    public function __construct($controller = '')
    {
        $this->_controller = $controller;
        $this->_viewRoot = ROOT . '/src/Views/';
        $this->setTemplate('default');
    }

    public function setTemplate($name)
    {
        $this->_template = $this->_viewRoot . 'Template/' . $name . '.php';
    }

    /**
     * Set Variables
     *
     * @param $name
     * @param $value
     */
    public function set($name,$value)
    {
        $this->variables[$name] = $value;
    }

    /**
     * Display Template
     *
     * @param string $viewName
     * @param array $data
     */
    function render($viewName = '', $data = array())
    {
        $viewName = ($viewName == '') ? $this->_controller : $viewName;
        $viewName = $viewName . 'View.php';

        if(!empty($data)){
            extract($data);
        }else{
            extract($this->variables);
        }

        if (file_exists($this->_viewRoot . $viewName)) {
            $_templatePageView = $this->_viewRoot . $viewName;
            //include $this->_viewRoot . $viewName . '.php';
            include $this->_template;
        } else {
            include ($this->_viewRoot . 'viewNotFound.php');
        }
    }

}