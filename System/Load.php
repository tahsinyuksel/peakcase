<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 21:53
 */

namespace System;


class Load{

    private static $instance;

    public $view;

    public function __construct(){
        self::$instance =& $this;
        $this->view = new View();
    }

    public static function instance(){
        if (!self::$instance){
            self::$instance = new Load();
        }
        return self::$instance;
    }

    public function view($file_name, $data = null){
        $this->view->render($file_name, $data);
    }

    public function model($name, $data = null){
        $model = ucfirst($name . 'Model');
        $class = '\\PeakCase\\Models\\' . $model;
        if(class_exists($class))
        {
            return new $class;
        }
        return null;
    }

    public function model2($file_name, $data = null, $class_name = '', $extension = '.php'){
        $file = ROOT . '/src/Models/' . $file_name . $extension;
        if( file_exists( $file ) && is_file( $file ) ){
            if( $class_name == '' )
                $class_name = $file_name ;

            include( $file );
            if( ! class_exists( $class_name ) )
                return false;

            $class_name = new $class_name();
            if( is_array( $data ) && count( $data ) > 0 ){
                foreach( $data as $key => $val ){
                    $class_name->$key = $val;
                }
            }
            return $class_name;
        }
        return false;
    }
}