<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 16:09
 */

namespace System;


class BaseController
{
    public $load;

    public function __construct()
    {
        $this->load = new Load();
    }

    /**
     * @param $name
     * @param $data
     * @return null
     */
    public function loadModel($name, $data = array())
    {
        return $this->load->model($name, $data);
    }

    public function loadView($file_name, $data = null){
        $this->load->view->render($file_name, $data);
    }

    public function jsonResponse($data, $headers = array())
    {
        header('Content-type: application/json');
        return json_encode($data);
    }

    public function isAjaxRequest()
    {
        if (strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest')
        {
            return true;
        }
        return false;
    }

    public function getUser()
    {
        if(isset($_SESSION['user']) && !empty($_SESSION['user'])){
            return $_SESSION['user'];
        }
        return false;
    }
}