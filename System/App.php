<?php
/**
 * Created by PhpStorm.
 * User: ubuntu7
 * Date: 19.12.2015
 * Time: 15:36
 */

namespace System;

use System\Router;

class App
{
    /**
     * @var null|Router
     */
    public $router = null;

    public function __construct()
    {
        session_start();
        $url = $_SERVER['REQUEST_URI'];
        $this->router = new Router($url);
        $_GET = $this->router->GetQueryParams();
    }

    public function run()
    {
        $controller = ucfirst($this->router->GetController() . 'Controller');
        $action = $this->router->GetAction() . 'Action';

        $c = '\\PeakCase\\Controllers\\' . $controller;
        if(! class_exists($c)){
            MyErrors::viewException('Route Not Found', '404');
            return false;
        }

        $n = new $c();
        if(method_exists($n, $action))
        {
            $n->{$action}();
        }
        else
        {
            MyErrors::viewException('Route Not Found', '404');
            return false;
        }
    }
}